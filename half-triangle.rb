=begin
Write a program that uses only two output statements a hash statements
and an end of line to produce a pattern of hash symbols shaped like a
sidewyas triangle
=end


1.upto(7) do |row|
  row_count = 4 - (4 - row).abs()

  for i in 1.upto(row_count)
    print "\#"
  end
  print "\n"
end
